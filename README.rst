========================================
XMLStarlet XML validation tool in Docker
========================================

This repository provides a starting point for `XMLStarlet <https://xmlstar.sourceforge.net/>`_ based validation using *Docker*.

For more documentation regarding the XMLStarlet XML validation tool see its documentation.


Docker Installation
===================

Have look at the official documentation on how to install Docker on:

- `Install Docker Engine on Linux <https://docs.docker.com/engine/install/>`_
- `Install Docker Desktop on Windows <https://docs.docker.com/desktop/install/windows-install>`_


How to build the Docker image
=============================

You can build the *Docker* image from the command line by using the following command::

    $ docker build -t xmlstarlet .

This will build an image with *xmlstarlet* tool and name it *xmlstarlet*.


Validation from CLI
===================

Now the *Docker* image is ready and can be used to validate an XML file.


To run the XML validation use the following command (according to your platform):

Linux::

    $ docker run -it --rm -v $PWD:/data xmlstarlet val --err --xsd schema/schema.xsd samples/accident.xml

    samples/accident.xml - valid


Windows::

    C:\DATEX2>docker run -it --rm -v %cd%:/data xmlstarlet val --err --xsd schema/schema.xsd samples/accident.xml

    samples/accident.xml - valid



Use a prebuilt Docker image
===========================

In order to simplify the XML validation, we offer a prebuilt *Docker* image already containing the *xmlstarlet* tool.

To use the prebuilt image, replace the *xmlstarlet* (the name of the image) in the above commands with the following ::

    registry.gitlab.com/tamtamresearch/personal/roman/xmlstarlet-docker:1.0.0


For example you use the following command on Windows::

    C:\DATEX2>docker run -it --rm -v %cd%:/data registry.gitlab.com/datex2/presentation/2023-virtduf-validation/xmlstarlet-docker:1.0.0 val --err --xsd schema/schema.xsd samples/accident.xml


Alternatively pull the prebuild *Docker* image and tag it as *xmlstarlet*::

    docker pull registry.gitlab.com/datex2/presentation/2023-virtduf-validation/xmlstarlet-docker:1.0.0
    docker image tag registry.gitlab.com/datex2/presentation/2023-virtduf-validation/xmlstarlet-docker:1.0.0 xmlstarlet


Now you can use the same commands as stated in the `Validation from CLI` chapter.



Changelog
=========

version 1.0.0 

- Initial version
