FROM alpine:3.18

RUN apk add --no-cache --update xmlstarlet \
	&& rm -rf /var/cache/apk/*

WORKDIR /data

ENTRYPOINT ["xmlstarlet"]
